<?php
/* function takes a timestamp or datetime and returns a string giving the time in human-readible relative form */
/* Similar to how Twitter displays timestamps */

function time_ago($t){
	//convert the param to unix timestamp
	$t = strtotime($t) ? strtotime($t) : $t;
	//first step, convert to minutes
	$minutes_ago = (time() - $t)/60;
	
	//if it was less than a minute ago, return
	if($minutes_ago <= 1) { return 'about 1 minute ago';}
	
	//if it was less than 1 hour ago, return
	if(ceil($minutes_ago) < 60) { return 'about '.ceil($minutes_ago).' '.pluralize(ceil($minutes_ago),'minute').' ago';}
	
	//if it was less than 1 day ago, return
	if(ceil($minutes_ago) < (60*23.5)) { return 'about '.ceil($minutes_ago/60).' '.pluralize(ceil($minutes_ago/60),'hour').' ago';}
	
	//if it was less than 1 week ago, return
	if(ceil($minutes_ago) < (60*24*6.5)) { return 'about '.ceil($minutes_ago/60/24).' '.pluralize(ceil($minutes_ago/60/24),'day').' ago';}
	
	//if it was less than 1 month ago, return
	if(ceil($minutes_ago) < (60*24*29.5)) { return 'about '.round($minutes_ago/60/24/7).' '.pluralize(round($minutes_ago/60/24/7),'week').' ago';}
	
	//if it was more than 1 month ago, return
	return 'more than 1 month ago';
	
}

//helper function
function pluralize($int,$str){
	if($int == 1){ return $str; }
	else{ return $str.'s'; }
}
?>